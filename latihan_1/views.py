from django.http import HttpResponse
from django.shortcuts import render


def latihan_1_main(request):
    ctx = {
        "title": "Render Data",
        "bahasa_program": ["Python", "Java", "Net", "VB", "C"],
        "list_hp": [
            {"nama": "Iphone", "id": 1},
            {"nama": "Samsung", "id": 2},
            {"nama": "Oppo", "id": 3},
            {"nama": "Nokia", "id": 4},
            {"nama": "Zenfone", "id": 5}
        ]
    }
    return render(request, 'tutorial_1/latihan_1.html', ctx)