from django.http import HttpResponse
from django.shortcuts import render
from django.core.urlresolvers import reverse


def home(request):
    list_url =[
        {"name": "Latihan 1", "url": reverse("latihan_1_main")}
    ]
    context = {"urls": list_url}
    return render(request, 'main.html', context)